package org.sid.trident;

import org.sid.trident.enums.ERole;
import org.sid.trident.models.Investor;
import org.sid.trident.models.OrganizationAccount;
import org.sid.trident.models.Role;
import org.sid.trident.models.User;
import org.sid.trident.repository.InvestorRepository;
import org.sid.trident.repository.OrganizationAccountRepository;
import org.sid.trident.repository.RoleRepository;
import org.sid.trident.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;
import java.util.stream.Stream;

@SpringBootApplication
@EnableJpaAuditing
public class TridentApplication {

    public static void main(String[] args) {
        SpringApplication.run(TridentApplication.class, args);
    }

    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowCredentials(false);
        //corsConfiguration.setAllowedOrigins(Arrays.asList("http://localhost:4200"));
        corsConfiguration.setAllowedOrigins(Arrays.asList("*"));
        corsConfiguration.setAllowedHeaders(Arrays.asList("Origin", "Access-Control-Allow-Origin", "Content-Type",
                "Accept", "Authorization", "Origin, Accept", "X-Requested-With",
                "Access-Control-Request-Method", "Access-Control-Request-Headers"));
        corsConfiguration.setExposedHeaders(Arrays.asList("Origin", "Content-Type", "Accept", "Authorization",
                "Access-Control-Allow-Origin", "Access-Control-Allow-Origin", "Access-Control-Allow-Credentials"));
        corsConfiguration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsFilter(urlBasedCorsConfigurationSource);
    }


/*    @Bean
    CommandLineRunner start(UserRepository userRepository, InvestorRepository investorRepository, PasswordEncoder encoder, RoleRepository roleRepository){
        Role investorRole = roleRepository.findByName(ERole.ROLE_INVESTOR).get();
        Role companyRole = roleRepository.findByName(ERole.ROLE_COMPANY).get();
        return args -> {

            // creation User
            User u1 = new User();
            u1.setUsername("test");
            u1.setEmail("email@email.com");
            u1.setPassword(encoder.encode("123456789"));
            u1.setRole(investorRole);
            User usersaved=userRepository.save(u1);
            // creation Investor

            Investor investor = new Investor();
            investor.setFirst_name("first name");
            investor.setLast_name("last name");
            investor.setPhone("phone  number");
            investor.setAdress("adresse");
            investor.setCity("city");
            investor.setHasWallet(false);









        };
    }*/


    @Bean
    CommandLineRunner start(RoleRepository roleRepository, OrganizationAccountRepository organizationAccountRepository) {

        return args -> {
            Stream.of("BITCOIN DEMO","GOLD DEMO", "SP500 DEMO").forEach(
                    s -> {
                        OrganizationAccount organizationAccount = new OrganizationAccount();
                        organizationAccount.setTitle(s);
                        organizationAccount.setAmount(0.0);
                        organizationAccount.setType("demo");
                        organizationAccountRepository.save(organizationAccount);
                    }
            );
            Stream.of("BITCOIN REAL","GOLD REAL", "SP500 REAL" ).forEach(
                    s -> {
                        OrganizationAccount organizationAccount = new OrganizationAccount();
                        organizationAccount.setTitle(s);
                        organizationAccount.setAmount(0.0);
                        organizationAccount.setType("real");
                        organizationAccountRepository.save(organizationAccount);
                    }
            );
            Stream.of(
                      ERole.ROLE_SUPER_ADMIN,
                      ERole.ROLE_OPERATOR,
                      ERole.ROLE_ADMIN,
                      ERole.ROLE_INVESTOR,
                      ERole.ROLE_COMPANY
                      ).forEach(r->{
                Role role = new Role();
                role.setName(r);
                roleRepository.save(role);
            });
        };
    }

}
