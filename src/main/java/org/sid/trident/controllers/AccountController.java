package org.sid.trident.controllers;

import org.sid.trident.models.InvestorAccount;
import org.sid.trident.models.OrganizationAccount;
import org.sid.trident.services.AccountsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {

    @Autowired
    AccountsService service;

    @GetMapping("/investor/{username}")
    public ResponseEntity<?> getAccountsForInvestor(@PathVariable String username){
        return  service.getInvestorAccounts(username);
    }

    @GetMapping("/sys/demo")
    public ResponseEntity<List<OrganizationAccount>> getDemoAccounts(){
        return  service.getOrganizationAccounts("demo");
    }
    @GetMapping("/sys/real")
    public ResponseEntity<List<OrganizationAccount>> getRealAccounts(){
        return  service.getOrganizationAccounts("real");
    }
}
