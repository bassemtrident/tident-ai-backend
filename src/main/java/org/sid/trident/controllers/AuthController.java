package org.sid.trident.controllers;

import javax.validation.Valid;

import org.sid.trident.payload.request.CompanySignupRequest;
import org.sid.trident.payload.request.InvestorSignupRequest;
import org.sid.trident.payload.request.LoginRequest;
import org.sid.trident.payload.request.SignupRequest;
import org.sid.trident.payload.response.JwtResponse;
import org.sid.trident.repository.CompanyRepository;
import org.sid.trident.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthService authService;
    @Autowired
    CompanyRepository companyRepository;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        JwtResponse response = authService.login(loginRequest);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/investor/signup")
    public ResponseEntity<?> registerInvestor(@Valid @RequestBody InvestorSignupRequest request) {
        return  authService.investorSingup(request);
    }

    @PostMapping("/company/signup")
    public ResponseEntity<?> registerCompany(@Valid @RequestBody CompanySignupRequest request) {
        return  authService.companySingup(request);
    }

    @GetMapping("/company")
    public ResponseEntity<?> registerCompany() {
        return  ResponseEntity.ok(companyRepository.findAll());
    }
}