package org.sid.trident.controllers;


import org.sid.trident.models.Company;
import org.sid.trident.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/company")
public class CompanyController {

    @Autowired
    CompanyService companyService;

    @GetMapping("/all")
    public ResponseEntity<List<Company>> getAll(){
        return ResponseEntity.ok(companyService.allCompany());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Company> getCompany(@PathVariable Long id){
        return ResponseEntity.ok(companyService.getCompanyById(id));
    }

    @GetMapping("/count")
    public ResponseEntity<Long> countCompany(){
        return ResponseEntity.ok(companyService.count());
    }
}
