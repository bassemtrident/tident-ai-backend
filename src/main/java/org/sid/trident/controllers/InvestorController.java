package org.sid.trident.controllers;

import org.sid.trident.services.InvestorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/investor")
public class InvestorController {

    @Autowired
    InvestorService service;

    @GetMapping("/profile/{username}")
    public ResponseEntity<?> getInvestorProfile(@PathVariable String username){
        return  service.getProfileInfo(username);
    }

    @GetMapping("/count")
    public ResponseEntity<Long> countCompany(){
        return ResponseEntity.ok(service.count());
    }

}
