package org.sid.trident.controllers;

import org.sid.trident.models.Loan;
import org.sid.trident.payload.request.LoanRequest;
import org.sid.trident.payload.response.LoanResponse;
import org.sid.trident.services.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/loan")
public class LoanController {

    @Autowired
    private LoanService loanService;

    @PostMapping("/create")
    public ResponseEntity<Loan> create(@RequestBody  LoanRequest loanRequest) {
        return loanService.createLoan(loanRequest);
    }


    @GetMapping("/all")
    public ResponseEntity<List<LoanResponse>> all() {
        return ResponseEntity.ok(loanService.allLoan());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Loan> getLoan(@PathVariable Long id) {
        return ResponseEntity.ok(loanService.getLoanById(id));
    }

    @GetMapping("/company/{username}")
    public ResponseEntity<List<LoanResponse>> getLoanByCompany(@PathVariable String username) {
        return ResponseEntity.ok(loanService.getLoanByCompanyUsername(username));
    }

    @GetMapping("/count")
    public ResponseEntity<Long> countCompany(){
        return ResponseEntity.ok(loanService.count());
    }

}
