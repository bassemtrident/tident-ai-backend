package org.sid.trident.controllers;

import lombok.Data;
import org.sid.trident.payload.request.Wallet;
import org.sid.trident.services.StratisRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/stratis")

public class StratisController {

    @Autowired
    private StratisRestService service;

    @GetMapping("/load")
    public ResponseEntity<?> getInvestorProfile(){
        return  ResponseEntity.ok("test "+service.postLoad());}

    @GetMapping("/mnemonic")
    public ResponseEntity<?> getMnemonic(){
        return  ResponseEntity.ok(service.getMnemonic());
    }

    @PostMapping("/wallet/create/{username}")
    public ResponseEntity<?> postCreateWallet(@RequestBody Wallet wallet, @PathVariable String username){
        return ResponseEntity.ok(service.postCreateWallet(wallet, username).getBody());
    }

}


