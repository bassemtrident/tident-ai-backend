package org.sid.trident.controllers;

import org.sid.trident.payload.request.PaymentUserToSystem;
import org.sid.trident.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/transaction")
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @PostMapping("/sys")
    public ResponseEntity paymentSys(@RequestBody PaymentUserToSystem request) {
        return  transactionService.paySys(request);
    }
    @GetMapping("/historical/{username}")
    public ResponseEntity historicalTransaction(@PathVariable String username) {
        return  transactionService.historicalTransaction(username);
    }
}
