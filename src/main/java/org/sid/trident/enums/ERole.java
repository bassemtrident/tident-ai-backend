package org.sid.trident.enums;

public enum ERole {
    ROLE_SUPER_ADMIN,
    ROLE_OPERATOR,
    ROLE_SUPPORT_OFFICER,
    ROLE_ADMIN,
    ROLE_COMPANY,
    ROLE_INVESTOR
}