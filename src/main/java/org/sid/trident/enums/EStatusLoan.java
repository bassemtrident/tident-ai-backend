package org.sid.trident.enums;

public enum EStatusLoan {
    PENDING,
    REJECTED,
    ACCEPTED,
    COMPLETED
}
