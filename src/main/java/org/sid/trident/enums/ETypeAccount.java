package org.sid.trident.enums;

public enum ETypeAccount {
    DEMO_ACCOUNT,
    REAL_ACCOUNT,
}
