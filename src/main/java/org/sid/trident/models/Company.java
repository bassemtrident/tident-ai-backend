package org.sid.trident.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.sid.trident.audit.Auditable;

import javax.persistence.*;

import java.util.List;
import java.util.Set;


@Entity
@Table(name = "company")
@Data
public class Company extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String website;
    private String city;
    private String country;
    private String sector;
    private String stage;
    @ElementCollection(targetClass=String.class)
    private List<String> type_of_business;
    private int revenue;
    private String Currency;
    private String description;
    private String logo;
    private String link_video;
    private String credit_scoring;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy="company")
    @JsonIgnore
    private Set<Loan> loans;



}
