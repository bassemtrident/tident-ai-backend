package org.sid.trident.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.sid.trident.audit.Auditable;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "investors")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Investor extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String first_name;
    private String last_name;
    private String phone;
    private String adress;
    private String city;


    @OneToMany(mappedBy = "investor")
    private List<InvestorAccount> accounts;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;
}
