package org.sid.trident.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.sid.trident.audit.Auditable;

import javax.persistence.*;

@Entity
@Table(name = "investor_accounts")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvestorAccount extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private double amount;

    @JsonIgnore
    @ManyToOne
    private Investor investor;



}
