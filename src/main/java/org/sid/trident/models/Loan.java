package org.sid.trident.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.sid.trident.audit.Auditable;
import org.sid.trident.enums.EStatusLoan;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "loan")
@Data
@AllArgsConstructor @NoArgsConstructor
public class Loan extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private String projectName;
    private double target;
    private double raised;
    private String currency;
    private String repayment_term;
    private String description;
    private Date due_date;
    private EStatusLoan status;

    @ManyToOne
    @JoinColumn(name="company_id", nullable=false)
    private Company company;
}
