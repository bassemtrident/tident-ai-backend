package org.sid.trident.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.sid.trident.audit.Auditable;

import javax.persistence.*;

@Entity
@Table(name = "organization_accounts")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationAccount extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private double amount;
    private String type;

}
