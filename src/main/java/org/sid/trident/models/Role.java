package org.sid.trident.models;
import lombok.Data;
import org.sid.trident.audit.Auditable;
import org.sid.trident.enums.ERole;

import javax.persistence.*;

@Entity
@Table(name = "roles")
@Data
public class Role extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private ERole name;
}
