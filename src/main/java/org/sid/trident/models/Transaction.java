package org.sid.trident.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.sid.trident.audit.Auditable;

import javax.persistence.*;

@Entity
@Table(name = "transactions")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transaction extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private double amount;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "from_id")
    private InvestorAccount from;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "to_id")
    private OrganizationAccount to;
}
