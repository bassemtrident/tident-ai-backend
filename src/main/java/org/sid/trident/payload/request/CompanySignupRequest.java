package org.sid.trident.payload.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class CompanySignupRequest {
    @NotBlank
    @Size(min = 3, max = 20)
    private String username;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    @NotBlank
    @Size(min = 6, max = 40)
    private String password;

    private String name;
    private String website;
    private String city;
    private String country;
    private String sector;
    private String stage;
    private List<String> type_of_business;
    private int revenue;
    private String Currency;
    private String description;
    private String logo;
    private String link_video;

}