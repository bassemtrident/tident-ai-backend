package org.sid.trident.payload.request;

import lombok.Data;
import org.sid.trident.enums.EStatusLoan;

import java.util.Calendar;
import java.util.Date;

@Data
public class LoanRequest {

    private String companyName;
    private String projectName;
    private double target;
    private double raised;
    private String currency;
    private String repayment_term;
    private String description;
    private Date due_date= dateafter60day();
    private EStatusLoan status;

    private Date dateafter60day() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 60);
        Date d = c.getTime();
        return  d;
    }
}
