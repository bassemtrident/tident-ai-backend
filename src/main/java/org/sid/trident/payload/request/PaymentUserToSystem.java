package org.sid.trident.payload.request;

import lombok.Data;


@Data
public class PaymentUserToSystem {
    private double amount;
    private long fromId;
    private long toId;


}
