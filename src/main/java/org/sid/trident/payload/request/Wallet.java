package org.sid.trident.payload.request;

import lombok.Data;

@Data
public class Wallet {
    private String name;
    private String password;
    private String passphrase;
}
