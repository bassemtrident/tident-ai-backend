package org.sid.trident.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.sid.trident.enums.EStatusLoan;
import org.sid.trident.models.Loan;

import java.util.Date;

@Data
@AllArgsConstructor @NoArgsConstructor
public class LoanResponse {
    private Long id;
    private String company_name;
    private String projectName;
    private double target;
    private double raised;
    private String currency;
    private String repayment_term;
    private String description;
    private Date due_date;
    private EStatusLoan status;
    private Date createdDate;
}
