package org.sid.trident.repository;

import org.sid.trident.models.InvestorAccount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InvestorAccountRepository extends JpaRepository<InvestorAccount, Long> {
   List<InvestorAccount> findAllByInvestor_User_Username(String username);

}
