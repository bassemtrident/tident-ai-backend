package org.sid.trident.repository;


import org.sid.trident.models.Investor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvestorRepository extends JpaRepository<Investor, Long> {
    Investor findInvestorByUser_Username(String username);

}
