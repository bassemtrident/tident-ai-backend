package org.sid.trident.repository;

import org.sid.trident.models.Loan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LoanRepository  extends JpaRepository<Loan, Long> {
    List<Loan> findAllByCompany_User_Username(String username);
}
