package org.sid.trident.repository;


import org.sid.trident.models.OrganizationAccount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrganizationAccountRepository extends JpaRepository<OrganizationAccount, Long> {

    List<OrganizationAccount> findAllByTypeOrderByCreatedDate(String type);
}
