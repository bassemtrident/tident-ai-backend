package org.sid.trident.repository;

import org.sid.trident.models.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    List<Transaction> findAllByFrom_Investor_User_Username(String username);
}
