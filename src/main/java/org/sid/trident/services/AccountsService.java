package org.sid.trident.services;

import org.sid.trident.models.OrganizationAccount;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AccountsService {
    ResponseEntity getInvestorAccounts(String username);
    ResponseEntity<List<OrganizationAccount>> getOrganizationAccounts(String type);
}
