package org.sid.trident.services;

import org.sid.trident.payload.request.CompanySignupRequest;
import org.sid.trident.payload.request.InvestorSignupRequest;
import org.sid.trident.payload.request.LoginRequest;
import org.sid.trident.payload.request.SignupRequest;
import org.sid.trident.payload.response.JwtResponse;
import org.springframework.http.ResponseEntity;

public interface AuthService {
    JwtResponse login(LoginRequest loginRequest);
    /*ResponseEntity singup(SignupRequest signupRequest);*/
    ResponseEntity investorSingup(InvestorSignupRequest request);
    ResponseEntity companySingup(CompanySignupRequest request);
}
