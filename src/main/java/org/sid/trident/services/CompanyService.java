package org.sid.trident.services;

import org.sid.trident.models.Company;

import java.util.List;

public interface CompanyService {
    List<Company> allCompany();
    Company getCompanyById( Long id);
    long count();
}
