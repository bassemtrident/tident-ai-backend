package org.sid.trident.services;

import org.sid.trident.models.Investor;
import org.sid.trident.models.InvestorAccount;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface InvestorService {
    ResponseEntity getAccounts(String username);
    ResponseEntity getProfileInfo(String username);
    Long count();


}
