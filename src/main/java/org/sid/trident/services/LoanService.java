package org.sid.trident.services;

import org.sid.trident.models.Loan;
import org.sid.trident.payload.request.LoanRequest;
import org.sid.trident.payload.response.LoanResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface LoanService {

    ResponseEntity<Loan> createLoan(LoanRequest loanRequest);
    List<LoanResponse> allLoan();
    Loan getLoanById(Long id);
    List<LoanResponse> getLoanByCompanyUsername(String usernmae);
    Long count();
}
