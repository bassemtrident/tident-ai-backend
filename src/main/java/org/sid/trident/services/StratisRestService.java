package org.sid.trident.services;

import org.sid.trident.models.User;
import org.sid.trident.payload.request.Wallet;
import org.sid.trident.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class StratisRestService {

    private final RestTemplate restTemplate;

    @Autowired
    private UserRepository userRepository;

    public StratisRestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public ResponseEntity<String> postLoad() {
        String url = "http://3.125.91.122:38223/api/Wallet/load";
        Map<String, Object> map = new HashMap<>();
        map.put("name", "cirrusdev");
        map.put("password", "password");
         ResponseEntity<String> response= this.restTemplate.postForEntity(url,map, String.class);
    return  response;
    }



    public ResponseEntity<String> postCreateWallet(Wallet wallet, String username) {
        String url = "http://3.125.91.122:38223/api/Wallet/create";
        Map<String, Object> map = new HashMap<>();
       /* String mnmonic = getMnemonic();
        System.out.println("MNONIC : "+mnmonic);*/
        map.put("password", wallet.getPassword());
        map.put("passphrase", wallet.getPassphrase());
        map.put("name", wallet.getName());
        ResponseEntity<String> response= this.restTemplate.postForEntity(url,map, String.class);
        if( response.getStatusCode().value() == 200){
            User user = userRepository.findByUsername(username).get();
            user.setHasWallet(true);
            userRepository.save(user);
        }
        return  response;
    }

    public String getMnemonic() {
        String url = "http://3.125.91.122:38223/api/Wallet/mnemonic?language=English&wordCount=12";
        ResponseEntity<String> response=this.restTemplate.getForEntity(url, String.class);
        return  response.getBody();
    }
}
