package org.sid.trident.services;

import org.sid.trident.payload.request.PaymentUserToSystem;
import org.springframework.http.ResponseEntity;

public interface TransactionService {
    ResponseEntity paySys(PaymentUserToSystem request);
    ResponseEntity historicalTransaction(String username);

}
