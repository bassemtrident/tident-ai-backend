package org.sid.trident.services.impl;

import org.sid.trident.models.OrganizationAccount;
import org.sid.trident.payload.response.MessageResponse;
import org.sid.trident.repository.InvestorAccountRepository;
import org.sid.trident.repository.OrganizationAccountRepository;
import org.sid.trident.repository.UserRepository;
import org.sid.trident.services.AccountsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountsServiceImpl  implements AccountsService {
    @Autowired
    InvestorAccountRepository investorAccountRepository;
    @Autowired
    OrganizationAccountRepository organizationAccountRepository;
    @Autowired
    UserRepository userRepository;

    @Override
    public ResponseEntity<?> getInvestorAccounts(String username) {
        if (!userRepository.existsByUsername(username)) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is not valid!"));
        }
        return ResponseEntity.ok(investorAccountRepository.findAllByInvestor_User_Username(username));

    }

    @Override
    public ResponseEntity<List<OrganizationAccount>> getOrganizationAccounts(String type) {
        return ResponseEntity.ok(organizationAccountRepository.findAllByTypeOrderByCreatedDate(type));
    }
}
