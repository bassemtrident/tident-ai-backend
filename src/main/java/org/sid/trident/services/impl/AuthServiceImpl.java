package org.sid.trident.services.impl;

import org.sid.trident.enums.ERole;
import org.sid.trident.models.*;
import org.sid.trident.payload.request.CompanySignupRequest;
import org.sid.trident.payload.request.InvestorSignupRequest;
import org.sid.trident.payload.request.LoginRequest;
import org.sid.trident.payload.request.SignupRequest;
import org.sid.trident.payload.response.JwtResponse;
import org.sid.trident.payload.response.MessageResponse;
import org.sid.trident.repository.*;
import org.sid.trident.security.jwt.JwtUtils;
import org.sid.trident.security.services.UserDetailsImpl;
import org.sid.trident.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;


@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;


    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    InvestorRepository investorRepository;
    @Autowired
    InvestorAccountRepository investorAccountRepository;
    @Autowired
    CompanyRepository companyRepository;

    @Override
    public JwtResponse login(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        return  new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                userDetails.getRole().name(),
                userDetails.isHasWallet()
               );
    }



    @Override
    public ResponseEntity investorSingup(InvestorSignupRequest request) {

        if (userRepository.existsByUsername(request.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }
        if (userRepository.existsByEmail(request.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }
        // Create new user's account
        User user = new User(request.getUsername(),
                request.getEmail(),
                encoder.encode(request.getPassword()));
        Role investorRole = roleRepository.findByName(ERole.ROLE_INVESTOR)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        user.setRole(investorRole);
        user.setHasWallet(false);
        User u=userRepository.save(user);
        Investor investor = new Investor();
        investor.setFirst_name(request.getFirst_name());
        investor.setLast_name(request.getLast_name());
        investor.setPhone(request.getPhone());
        investor.setAdress(request.getAdress());
        investor.setCity(request.getCity());
        investor.setUser(u);
        Investor investorSaved = investorRepository.save(investor);
        InvestorAccount demo = new InvestorAccount();
        demo.setAmount(1000.0);
        demo.setTitle("DEMO ACCOUNT");
        demo.setInvestor(investorSaved);
        investorAccountRepository.save(demo);
        InvestorAccount real = new InvestorAccount();
        real.setAmount(0.0);
        real.setTitle("REAL ACCOUNT");
        real.setInvestor(investorSaved);
        investorAccountRepository.save(real);
        return ResponseEntity.ok(new MessageResponse("Investor registered successfully!"));
    }

    @Override
    public ResponseEntity companySingup(CompanySignupRequest request) {
        if (userRepository.existsByUsername(request.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }
        if (userRepository.existsByEmail(request.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }
        User user = new User(request.getUsername(),
                request.getEmail(),
                encoder.encode(request.getPassword()));
        Role investorRole = roleRepository.findByName(ERole.ROLE_COMPANY)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        user.setRole(investorRole);
        User u=userRepository.save(user);

        Company company = new Company();
        company.setName(request.getName());
        company.setWebsite(request.getWebsite());
        company.setCity(request.getCity());
        company.setCountry(request.getCountry());
        company.setSector(request.getSector());
        company.setStage(request.getStage());
        company.setType_of_business(request.getType_of_business());
        company.setRevenue(request.getRevenue());
        company.setCurrency(request.getCurrency());
        company.setDescription(request.getDescription());
        company.setLogo(request.getLogo());
        company.setLink_video(request.getLink_video());

        company.setUser(u);
        companyRepository.save(company);

        return ResponseEntity.ok(new MessageResponse("Company registered successfully!"));
    }


}
