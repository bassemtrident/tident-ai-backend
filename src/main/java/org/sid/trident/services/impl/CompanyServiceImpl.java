package org.sid.trident.services.impl;

import org.sid.trident.models.Company;
import org.sid.trident.repository.CompanyRepository;
import org.sid.trident.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    CompanyRepository companyRepository;
    @Override
    public List<Company> allCompany() {
        return companyRepository.findAll();
    }

    @Override
    public Company getCompanyById(Long id) {
        return companyRepository.findById(id).get();
    }

    @Override
    public long count() {
        return companyRepository.count();
    }
}
