package org.sid.trident.services.impl;

import org.sid.trident.models.Investor;
import org.sid.trident.models.InvestorAccount;
import org.sid.trident.payload.response.MessageResponse;
import org.sid.trident.repository.InvestorAccountRepository;
import org.sid.trident.repository.InvestorRepository;
import org.sid.trident.repository.UserRepository;
import org.sid.trident.services.InvestorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvestorServiceImpl implements InvestorService {
    @Autowired
    InvestorAccountRepository investorAccountRepository;
    @Autowired
    InvestorRepository investorRepository;
    @Autowired
    UserRepository userRepository;

    @Override
    public ResponseEntity<?> getAccounts(String username) {
        if (!userRepository.existsByUsername(username)) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is not valid!"));
        }
        return ResponseEntity.ok(investorAccountRepository.findAllByInvestor_User_Username(username));
    }

    @Override
    public ResponseEntity<?> getProfileInfo(String username) {
        if (!userRepository.existsByUsername(username)) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is not valid!"));
        }
        return ResponseEntity.ok(investorRepository.findInvestorByUser_Username(username));

    }

    @Override
    public Long count() {
        return investorRepository.count();
    }
}
