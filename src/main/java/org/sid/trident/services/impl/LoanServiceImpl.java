package org.sid.trident.services.impl;

import org.sid.trident.models.Company;
import org.sid.trident.models.Loan;
import org.sid.trident.payload.request.LoanRequest;
import org.sid.trident.payload.response.LoanResponse;
import org.sid.trident.repository.CompanyRepository;
import org.sid.trident.repository.LoanRepository;
import org.sid.trident.services.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LoanServiceImpl implements LoanService {

    @Autowired
private LoanRepository loanRepository;
    @Autowired
    private CompanyRepository companyRepository;
    @Override
    public ResponseEntity<Loan> createLoan(LoanRequest loanRequest) {
        Loan loan = new Loan();
        loan.setProjectName(loanRequest.getProjectName());
        loan.setTarget(loanRequest.getTarget());
        loan.setRepayment_term(loanRequest.getRepayment_term());
        loan.setDescription(loanRequest.getDescription());
        loan.setDue_date(loanRequest.getDue_date());
        loan.setCurrency(loanRequest.getCurrency());
        loan.setRaised(loanRequest.getRaised());
        loan.setStatus(loanRequest.getStatus());
        Company company = companyRepository.findByName(loanRequest.getCompanyName());
        loan.setCompany(company);
        return ResponseEntity.ok(loanRepository.save(loan));
    }

    @Override
    public List<LoanResponse> allLoan() {
        List<Loan> loans = loanRepository.findAll();
        List<LoanResponse> loanResponsesList = new ArrayList<>();
        loans.forEach(loan -> {
            LoanResponse loanResponse= new LoanResponse();
            loanResponse.setId(loan.getId());
            loanResponse.setCompany_name(loan.getCompany().getName());
            loanResponse.setProjectName(loan.getProjectName());
            loanResponse.setTarget(loan.getTarget());
            loanResponse.setRaised(loan.getRaised());
            loanResponse.setCurrency(loan.getCurrency());
            loanResponse.setDescription(loan.getDescription());
            loanResponse.setStatus(loan.getStatus());
            loanResponse.setRepayment_term(loan.getRepayment_term());
            loanResponse.setDue_date(loan.getDue_date());
            loanResponsesList.add(loanResponse);

        });
        return loanResponsesList;
    }

    @Override
    public Loan getLoanById(Long id) {
        return loanRepository.findById(id).get();
    }

    @Override
    public List<LoanResponse> getLoanByCompanyUsername(String username) {
        List<Loan> loans = loanRepository.findAllByCompany_User_Username(username);
        List<LoanResponse> loanResponsesList = new ArrayList<>();
        loans.forEach(loan -> {
            LoanResponse loanResponse= new LoanResponse();
            loanResponse.setId(loan.getId());
            loanResponse.setCompany_name(loan.getCompany().getName());
            loanResponse.setProjectName(loan.getProjectName());
            loanResponse.setTarget(loan.getTarget());
            loanResponse.setRaised(loan.getRaised());
            loanResponse.setCurrency(loan.getCurrency());
            loanResponse.setDescription(loan.getDescription());
            loanResponse.setStatus(loan.getStatus());
            loanResponse.setRepayment_term(loan.getRepayment_term());
            loanResponse.setDue_date(loan.getDue_date());
            loanResponse.setCreatedDate(loan.getCreatedDate());
            loanResponsesList.add(loanResponse);

        });
        return loanResponsesList;
    }

    @Override
    public Long count() {
        return loanRepository.count();
    }
}
