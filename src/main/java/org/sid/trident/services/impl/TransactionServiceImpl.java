package org.sid.trident.services.impl;

import org.sid.trident.models.InvestorAccount;
import org.sid.trident.models.Transaction;
import org.sid.trident.models.OrganizationAccount;
import org.sid.trident.payload.request.PaymentUserToSystem;
import org.sid.trident.payload.response.MessageResponse;
import org.sid.trident.repository.InvestorAccountRepository;
import org.sid.trident.repository.TransactionRepository;
import org.sid.trident.repository.OrganizationAccountRepository;
import org.sid.trident.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {
    @Autowired
    InvestorAccountRepository investorAccountRepository;
    @Autowired
    OrganizationAccountRepository organizationAccountRepository;
    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public ResponseEntity paySys(PaymentUserToSystem request) {
        InvestorAccount from = investorAccountRepository.findById(request.getFromId()).get();
        OrganizationAccount to = organizationAccountRepository.findById(request.getToId()).get();
        from.setAmount(from.getAmount()-request.getAmount());
        investorAccountRepository.save(from);
        to.setAmount(request.getAmount());
        organizationAccountRepository.save(to);
        Transaction transaction = new Transaction();
        transaction.setAmount(request.getAmount());
        transaction.setFrom(from);
        transaction.setTo(to);
        transactionRepository.save(transaction);
        return ResponseEntity.ok(new MessageResponse("payment successfully!"));
    }

    @Override
    public ResponseEntity historicalTransaction(String username) {
        List<Transaction> transactions = transactionRepository.findAllByFrom_Investor_User_Username(username);
        return ResponseEntity.ok(transactions);
    }
}
